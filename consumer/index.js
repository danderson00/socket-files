module.exports = () => () => ({
  name: 'files',
  initialise: ({ api }) => ({
    middleware: {
      uploadFiles: async (_, files) => {
        const { url, method } = await api.requestUpload()
        const form = new FormData()
        Array.from(files).forEach(file => form.append(file.name, file))
        return fetch(url, { method, body: form }).then(res => res.json())
      },
      uploadFilesDialog: () => openFileDialog().then(files => api.uploadFiles(files))
    }
  })
})

const openFileDialog = () => new Promise((resolve, reject) => {
  const input = document.createElement('input')
  input.id = '__socket_files__'
  input.type = 'file'
  input.style.position = 'absolute'
  input.style.top = '-10000px'
  document.body.appendChild(input)

  const cleanup = () => {
    document.body.onfocus = undefined
    document.body.removeChild(input)
  }

  input.addEventListener('change', () => {
    cleanup()
    if(input.files.length > 0) {
      resolve(input.files)
    } else {
      reject('No files selected')
    }
  })

  document.body.onfocus = () => {
    cleanup()
    reject('Dialog cancelled')
  }

  input.click()
})
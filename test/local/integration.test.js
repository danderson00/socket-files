const hostModule = require('@x/socket/host')
const consumerModule = require('@x/socket/consumer')
const hostFeature = require('../../host')
const { createServer } = require('http')
const { createReadStream } = require('fs')
const WebSocket = require('ws')
const fetch = require('node-fetch')
const FormData = require('form-data')
const rimraf = require('rimraf')

let server, httpServer

const setup = () => {
  httpServer = createServer()
  server = new WebSocket.Server({ server: httpServer })
  hostModule({ server, httpServer, log: { level: 'fatal' } })
    .useFeature(hostFeature({ provider: 'local', storagePath: __dirname + '/uploads' }))
  httpServer.listen(1234)
  return consumerModule({ socketFactory: () => new WebSocket('ws://localhost:1234') }).connect()
}

afterEach(done => {
  server.close()
  httpServer.close()
  rimraf(__dirname + '/uploads', done)
})

test("basic upload / download", async () => {
  const api = await setup()
  const token = await api.requestUpload()
  const uploadResult = await upload(token, __dirname + '/test.txt')
  expect(uploadResult).toEqual({ success: true, urls: { 'test.txt': 'http://localhost:1234/uploads/test.txt' } })
  expect((await download(uploadResult.urls['test.txt'])).trim()).toEqual('Hello')
})

const upload = (token, path) => {
  const body = new FormData()
  body.append('file', createReadStream(path))
  return fetch(token.url, { method: token.method, body }).then(res => res.json())
}

const download = url => fetch(url, { method: 'GET' }).then(res => res.text())
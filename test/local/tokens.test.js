const tokens = require('../../host/local/tokens')

const data = { p1: 1 }

test("data is returned on burn", () => {
  const { mint, burn } = tokens()
  const id = mint(data)
  expect(burn(id)).toEqual(data)
})

test("tokens cannot be burned twice", () => {
  const { mint, burn } = tokens()
  const id = mint(data)
  burn(id)
  expect(() => burn(id)).toThrow()
})

test("tokens can no longer be burned after expiry", async () => {
  const { mint, burn } = tokens()
  const id = mint(data)
  await new Promise(r => setTimeout(r))
  expect(() => burn(id)).toThrow()
})
const { v4: uuid } = require('uuid')

module.exports = (options = {}) => {
  const { expiry } = options

  const tokens = {}

  const expiryTimer = id => setTimeout(() => delete tokens[id], expiry)

  return {
    mint: data => {
      const id = uuid()
      tokens[id] = { data, timer: expiryTimer(id) }
      return id
    },
    burn: id => {
      const { data, timer } = tokens[id] || {}
      if(!data) {
        throw new Error('No such token')
      }
      delete tokens[id]
      clearTimeout(timer)
      return data
    }
  }
}
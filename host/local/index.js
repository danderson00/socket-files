const tokenRepository = require('./tokens')
const http = require('./http')
const { mkdirSync } = require('fs')

const defaultOptions = {
  expiry: 5000,
  allowMultiple: true,
  uploadMethod: 'POST',
  requestPath: '/uploads',
  storagePath: 'uploads',
  requireAuthentication: false,
  ssl: false,
  cors: false
}

module.exports = (httpServer, userOptions, log) => {
  if(!httpServer) {
    throw new Error('Local storage specified but no httpServer configured')
  }

  const options = { ...defaultOptions, ...userOptions }
  const { storagePath, ssl } = options

  mkdirSync(storagePath, { recursive: true })
  const tokens = tokenRepository(options)
  const { tokenToRequest } = http(httpServer, tokens, options, log)

  return {
    api: {
      requestUpload: () => {},
      uploadFiles: () => { throw new Error('You must load the @x/socket.files consumer feature')},
      uploadFilesDialog: () => { throw new Error('You must load the @x/socket.files consumer feature')}
    },
    middleware: {
      requestUpload: ({ connection: { user, request } }) => {
        if(options.requireAuthentication && (!user || !user.username)) {
          throw new Error('Not logged in')
        }
        return tokenToRequest(request.headers.host, tokens.mint({ user }))
      }
    }
  }
}


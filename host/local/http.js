const formidable = require('formidable')
const serveStatic = require('serve-static')
const finalhandler = require('finalhandler')
const { mv } = require('./utilities')
const { resolve } = require('path')

module.exports = (httpServer, tokens, options, log) => {
  const { allowMultiple, uploadMethod, requestPath, storagePath, ssl, cors } = options

  const serve = serveStatic(storagePath)

  const hostUrl = (host, path) => `http${ssl ? 's' : ''}://${host}${path}`
  const resourceUrl = (host, filename) => hostUrl(host, `${requestPath}/${filename}`)

  const tokenToRequest = (host, id) => ({
    url: hostUrl(host, `${requestPath}?token=${id}`),
    method: uploadMethod
  })

  const filesToUrls = (host, files) => Object.values(files).reduce(
    (files, file) => ({
      ...files,
      [file.name]: resourceUrl(host, file.name)
    }),
    {}
  )

  httpServer.on('request', (req, res) => {
    const writeJson = (responseCode, source) => res
      .writeHead(responseCode, { 
        'Content-Type': 'application/json',
        ...(cors && {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': `${uploadMethod},GET`,
          'Access-Control-Allow-Headers': 'Content-Type',
        })
      })
      .end(JSON.stringify(source, null, 2))
    const writeError = error => writeJson(500, { success: false, error: error.message })
    const writeBadRequest = error => writeJson(400, { success: false, error })

    if (req.url.startsWith(requestPath)) {
      if(req.method.toLowerCase() === uploadMethod.toLowerCase()) {
        const token = new URL(hostUrl(req.headers.host, req.url)).searchParams.get('token')
        if(!token) {
          writeBadRequest('No token provided')
        }

        try {
          const tokenData = tokens.burn(token)
          formidable({ multiples: allowMultiple }).parse(req, (err, fields, files) => {
            if (err) {
              writeError(err)
              return
            }

            if(Object.keys(files).length === 0) {
              writeBadRequest('No files uploaded')
              return
            }

            Promise.all(Object.values(files).map(file => mv(file.path, resolve(storagePath, file.name))))
              .then(() => writeJson(200, {
                success: true,
                urls: filesToUrls(req.headers.host, files)
              }))
              .catch(writeError)
          })

        } catch(e) {
          writeError(e)
        }
      } else if(req.method.toLowerCase() === 'options' && cors) {
        writeJson(204, '')
      } else {
        req.url = req.url.substring(requestPath.length)
        serve(req, res, finalhandler(req, res))
      }
    }
  })

  return { tokenToRequest }
}


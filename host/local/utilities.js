const { rename } = require('fs')

module.exports = {
  mv: (oldPath, newPath) => new Promise((resolve, reject) => {
    rename(oldPath, newPath, err => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
}
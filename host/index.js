const providers = {
  local: require('./local')
}

module.exports = options => ({ httpServer, log }) => {
  const { api, middleware } = providers[options.provider](httpServer, options, log)
  return {
    name: 'files',
    api,
    middleware
  }
}
